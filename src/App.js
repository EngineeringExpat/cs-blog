import React, { useState, useEffect } from 'react';
import ReactMarkdown from 'react-markdown';

import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Engineering Expat</h1>
      <Posts />
    </div>
  );
}

function Posts(props) {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()
  useEffect(() => {
    fetch("https://blog.engineeringexpat.com/prod/posts")
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result.Items);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [])

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <div>
        {items.map(item => (
          <div key={item.time.N}>
          <hr />
          <h1>{item.title.S}</h1>
          <p>{(new Date(item.time.N * 1000)).toLocaleDateString()}</p>
          <ReactMarkdown children={item.body.S} />
          </div>
        ))}
      </div>
    );
  }
}

export default App;
